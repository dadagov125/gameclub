﻿namespace GameClub.Common.Models
{
    public class ClientModel
    {
        public string Id { get; set; }
        
        public string IpAddress { get; set; }
        
        public string ProcessorId { get; set; }
        
        public string Name { get; set; }
        
        public bool IsBlocked { get; set; }
    }
}