﻿using GameClub.Common.Models;

namespace GameClub.Common.Actions
{
    public class CreateClientAction:IAction<ClientModel>
    {
        public string Type { get; }
        public ClientModel Data { get; set; }

        public CreateClientAction()
        {
            Type=ActionTypes.CreateClientAction;
        }
    }
}