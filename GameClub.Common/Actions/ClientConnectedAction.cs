﻿using GameClub.Common.Models;

namespace GameClub.Common.Actions
{
    public class ClientConnectedAction:IAction<ClientModel>
    {
        public string Type { get; }
        public ClientModel Data { get; set; }

        public ClientConnectedAction()
        {
            Type = ActionTypes.ClientConnectedAction;
        }
        
    }
}