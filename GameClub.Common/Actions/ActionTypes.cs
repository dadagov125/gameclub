﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameClub.Common.Actions
{
    public  class ActionTypes
    {
        public  const string TEST="TEST";


        public const string CreateClientAction = nameof(CreateClientAction);

        public const string ClientConnectedAction = nameof(ClientConnectedAction);
    }
}
