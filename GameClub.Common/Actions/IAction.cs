﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameClub.Common.Actions
{
    public interface IAction<T>: IAction
    {
        T Data { get; set; }
    }


    public interface IAction
    {
        string Type { get;  }
        
    }

    public static class ActionExtention
    {
        public static T GetData<T>(this IAction action)
        {
            return ((IAction<T>)action).Data;
        }
    }
}
