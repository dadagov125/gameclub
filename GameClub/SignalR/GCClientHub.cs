﻿using System.Linq;
using System.Threading.Tasks;
using GameClub.Common.Actions;
using GameClub.Common.Models;
using GameClub.Data;
using GameClub.Entities;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.SignalR;

namespace GameClub.SignalR
{
    public class GCClientHub : Hub
    {
        private ApplicationContext _context;

        private AdminHub _adminHub;
        public GCClientHub(ApplicationContext context, AdminHub adminHub)
        {
            _context = context;
            _adminHub = adminHub;
        }

        public async Task Confirm(string processorId)
        {
            if (string.IsNullOrWhiteSpace(processorId))
            {
                 await this.Clients.Caller.SendAsync("NoConfirmed", "ProcessorId is not present");    
                 return;
            }

            var connectionFeature = Context.Features.Get<IHttpConnectionFeature>();
            var remoteIpAddress = connectionFeature.RemoteIpAddress.ToString();
            var client = _context.Clients
                .FirstOrDefault(c =>c.IsDeleted==false &&
                                    c.ProcessorId == processorId);

            if (client == null)
            {
                client=new Client()
                {
                    IpAddress = remoteIpAddress,
                    IsBlocked = true,
                    IsDeleted = false,
                    ProcessorId = processorId,
                    Name = null
                };
                _context.Clients.Add(client);
               await _context.SaveChangesAsync();

                var createClientAction = new CreateClientAction()
                {
                    Data = new ClientModel()
                    {
                        Id = client.Id,
                        IpAddress = client.IpAddress,
                        IsBlocked = client.IsBlocked,
                        Name = client.Name,
                        ProcessorId = client.ProcessorId
                    }
                };
               await _adminHub.Clients.All.SendAsync("Action",createClientAction);
            }
            else if (client.IpAddress != remoteIpAddress)
            {
                client.IpAddress = remoteIpAddress;
                await _context.SaveChangesAsync();
                var conflictedClients = _context.Clients
                    .Where(c => c.IsDeleted==false &&
                                c.IpAddress == remoteIpAddress)
                    .ToList();
                
//                if (conflictedClients.Count>1)
//                {
//                    foreach (var cc in conflictedClients)
//                    {
//                        cc.IsBlocked = true;
//                    }
//
//                    await  _context.SaveChangesAsync();
//                    //TODO: notify discovered conflicted clients
//                }

            }

            await this.Clients.Caller.SendAsync("Confirmed", processorId);
            var clienConnectedAction=new ClientConnectedAction()
            {
                Data = new ClientModel()
                {
                    Id = client.Id,
                    IpAddress = client.IpAddress,
                    IsBlocked = client.IsBlocked,
                    Name = client.Name,
                    ProcessorId = client.ProcessorId
                }
            };
           await _adminHub.Clients.All.SendAsync("Action", clienConnectedAction);
        }
    }
}