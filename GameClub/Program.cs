
using System.Linq;
using System.Net.Sockets;
using Makaretu.Dns;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace GameClub
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            var service = "game_club.service.local";
            var mdns = new MulticastService();
            mdns.QueryReceived += (s, e) =>
            {
                var msg = e.Message;
                if (msg.Questions.Any(q => q.Name == service && q.Type == DnsType.A))
                {
                    var a = MulticastService.GetLinkLocalAddresses()
                        .Where(ip=>ip.AddressFamily==AddressFamily.InterNetwork);
                    var addresses = MulticastService.GetIPAddresses()
                        .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                    
                    var res = msg.CreateResponse();
                    foreach (var address in addresses)
                    {
                        res.Answers.Add(new ARecord
                        {
                            Name = service,
                            Address = address,
                        });
                    }
                    mdns.SendAnswer(res);
                }
            };
            mdns.Start();
            
            var https = MulticastService.GetIPAddresses()
                .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .Select(a=>$"https://{a}:5001")
                .Concat(new []{"https://localhost:5001"})
                .ToArray();
            var http = MulticastService.GetIPAddresses()
                .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .Select(a=>$"http://{a}:5000")
                .Concat(new []{"http://localhost:5000"})
                .ToArray();
            
            CreateWebHostBuilder(args)
                .UseUrls(https.Concat(http).ToArray())
                .Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}