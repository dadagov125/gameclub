﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GameClub.Entities;
using GameClub.Entities.Common;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Newtonsoft.Json;

namespace GameClub.Data
{
    public  class ApplicationContext:IdentityDbContext<User>
    {
        public virtual DbSet<Client> Clients { get; set; }

        public virtual DbSet<Tariff> Tariffs { get; set; }
        
        public virtual DbSet<SessionPayment> SessionPayments { get; set; }
        
        public virtual DbSet<Session> Sessions { get; set; }
        
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options):base(options)
        {
            Database.EnsureCreated();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tariff>()
                .Property(e=>e.TariffSettings)
                .HasConversion(v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), 
                    v => JsonConvert.DeserializeObject<IList<TariffSetting>>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));

//
//            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole { Name = "Admin", NormalizedName = "Admin".ToUpper() });
//            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole { Name = "Operator", NormalizedName = "Operator".ToUpper() });
//            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole { Name = "User", NormalizedName = "User".ToUpper() });
//
//            
            
            base.OnModelCreating(modelBuilder);
        }
        
        public override int SaveChanges()
        {
            PrepareEntries();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            PrepareEntries();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            PrepareEntries();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            PrepareEntries();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }
        
        private void PrepareEntries()
        {
            foreach (var entry in ChangeTracker.Entries().Where(e=>e.State==EntityState.Deleted))
            {
                if (entry.Entity is IDeletable)
                {
                    IDeletable deletable =(IDeletable) entry.Entity;
                    deletable.IsDeleted = true;
                    entry.State = EntityState.Modified;
                }
            }
        }
        
        
    }
}