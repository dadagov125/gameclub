import React from 'react';

import { connect } from 'react-redux';
import Button from "@material-ui/core/Button";
import {HubConnectionBuilder, HubConnection, LogLevel} from "@aspnet/signalr"

class Home extends React.Component{
    connection;
    constructor(props){
        super(props)
    }

    componentDidMount() {
        let hubUrl = 'https://localhost:5001/AdminHub';

        this.connection= new HubConnectionBuilder()
           .withUrl(hubUrl)
           .configureLogging(LogLevel.Information)
           .build();
        
        this.connection.on("Send", (data)=>{
            console.log("Принято сообщение:", data)
        });
        this.connection.start()
    }
    
    componentWillUnmount() {
    }

    render() {
        
        return (
            <div>
                <h1>Hello, world!</h1>
               




                <Button onClick={()=>{
                    this.connection.invoke("Send", "Hello");
                }} variant="contained" color="primary">
                    Hello World
                </Button>
            </div>
        );
    }


}

export default connect()(Home);
