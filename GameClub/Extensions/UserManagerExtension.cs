﻿using System.Threading.Tasks;
using GameClub.Entities;
using Microsoft.AspNetCore.Identity;

namespace GameClub.Extensions
{
    public static class UserManagerExtension
    {
        public static async Task SeedUsersAsync(this UserManager<User> userManager)
        {
            if (await userManager.FindByEmailAsync("admin@local.com") ==null)
            {
                var user = new User
                {
                    UserName = "admin@local.com",
                    Email = "admin@local.com"
                };
                IdentityResult result = await userManager.CreateAsync(user, "Admin");
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }       
        } 
        
        public static async Task SeedRolesAsync(this RoleManager<IdentityRole> roleManager)
        {
            string [] roles=new string []
            {
                "Admin",
                "Operator",
                "User"
            };

            foreach (var role in roles)
            {
                if (!await roleManager.RoleExistsAsync(role))
                {
                   await roleManager.CreateAsync(new IdentityRole{Name = role, NormalizedName = role.ToUpper()});
                }
            }
        } 
    }
}