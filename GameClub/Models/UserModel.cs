﻿using GameClub.Entities;

namespace GameClub.Models
{
    public class UserModel
    {
        public UserModel()
        {
        }

        public UserModel(User user, string role = null)
            : this(user.Id,  user.UserName,  role)
        {
        }

        public UserModel(string id, string userName,  string role)
        {
            Id = id;
            UserName = userName;
            Role = role;
        }

        public string Id { get; set; }

        public string Role { get; set; }

        public string UserName { get; set; }


    }
}