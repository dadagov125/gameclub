﻿using System.Linq;
using System.Threading.Tasks;
using GameClub.Entities;
using GameClub.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace GameClub.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;

        private readonly RoleManager<IdentityRole> _roleManager;

        public UserController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }
        
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            if (ModelState.IsValid)
            {
                SignInResult result =
                    await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    
                    var user = await _userManager.FindByEmailAsync(model.Email);


                    if (user == null)
                    {
                        return NotFound();
                    }

                    var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

                    return Ok(new UserModel(user, role));
                }

                if (result.IsLockedOut)
                {
                    ModelState.AddModelError("IsLockedOut", "true");
                }
                else if (result.IsNotAllowed)
                {
                    ModelState.AddModelError("IsNotAllowed", "true");
                }
                else if (result.RequiresTwoFactor)
                {
                    ModelState.AddModelError("RequiresTwoFactor", "true");
                }
                else
                {
                    ModelState.AddModelError("LoginOrPassword", "failed");
                }
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }
        
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Me()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized();
            }

            var user = await _userManager.FindByEmailAsync(User.Identity.Name);


            if (user == null)
            {
                return NotFound();
            }

            var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();

            return Ok(new UserModel(user, role));
        }


    }
}