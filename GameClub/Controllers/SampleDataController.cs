using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameClub.Data;
using GameClub.Entities;
using Makaretu.Dns;
using Microsoft.AspNetCore.Mvc;

namespace GameClub.Controllers
{
    [Route("api/[controller]/[action]")]
    public class SampleDataController : Controller
    {
        private ApplicationContext ctx;
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public SampleDataController(ApplicationContext ctx)
        {
            this.ctx = ctx;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> WeatherForecasts(int startDateIndex)
        {
//            ctx.Tariffs.Add(new Tariff()
//            {
//                Name = "Test",
//                TariffSettings = new List<TariffSetting>()
//                {
//                    new TariffSetting()
//                    {
//                        Name = "aas",
//                        Price = 22.3m,
//                        Days =new HashSet<DayOfWeek>() {DayOfWeek.Friday,DayOfWeek.Monday, DayOfWeek.Thursday }
//                    }
//                }
//            });
//   
//            ctx.SaveChanges();
//          var list=  ctx.Tariffs.ToList();


            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index + startDateIndex).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get { return 32 + (int) (TemperatureC / 0.5556); }
            }
        }
    }
}