﻿using System;
using GameClub.Entities.Common;

namespace GameClub.Entities
{
    public class Client:Identity, IDeletable
    {
        public string Id { get; set; }
        
        public string IpAddress { get; set; }
        
        public string ProcessorId { get; set; }
        
        public string Name { get; set; }
        
        public bool IsBlocked { get; set; }
        
        public bool IsDeleted { get; set; }
        public Client()
        {
            Id = Guid.NewGuid().ToString();
        }

        
    }
}