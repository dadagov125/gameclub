﻿using System;

namespace GameClub.Entities.Common
{
    public interface Identity
    {
        string Id { get; set; }
    }
}