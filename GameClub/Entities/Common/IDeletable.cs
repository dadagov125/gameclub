﻿namespace GameClub.Entities.Common
{
    public interface IDeletable
    {
        bool IsDeleted { get;  set; }
    }
}