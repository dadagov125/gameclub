﻿namespace GameClub.Entities
{
    public enum SessionState
    {
        Online,
        Paused,
        Completed
    }
}