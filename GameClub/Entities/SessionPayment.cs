﻿using System;
using GameClub.Entities.Common;

namespace GameClub.Entities
{
    public class SessionPayment:Identity, IDeletable
    {
        public string Id { get; set; }
        public Session Session { get; set; }
        public decimal Amount { get; set; }
        public PaymentType PaymentType { get; set; }
        public User CreatedOn { get; set; }
        public DateTime CreatedAt { get; set; }
        public Tariff Tariff { get; set; }
        public bool IsDeleted { get; set; }
        
        public SessionPayment()
        {
            Id = Guid.NewGuid().ToString();
        }

   
    }
}