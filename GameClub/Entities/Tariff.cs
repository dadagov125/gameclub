﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameClub.Entities.Common;

namespace GameClub.Entities
{
    public class Tariff:Identity, IDeletable
    {

        public string Id { get; set; }
        
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        public IList<TariffSetting> TariffSettings { get; set; }
        public Tariff()
        {
            Id = Guid.NewGuid().ToString();
        }

        
    }

}