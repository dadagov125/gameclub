﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using GameClub.Entities.Common;
using Newtonsoft.Json;

namespace GameClub.Entities
{
    public class TariffSetting
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime From { get; set; }=new DateTime(0,0,0,0,0,0);
        public DateTime To { get; set; }=new DateTime(0,0,0,23,59,59);
        public ISet<DayOfWeek> Days { get; set; }
  
    }
}