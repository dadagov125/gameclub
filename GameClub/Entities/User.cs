﻿using System.Collections;
using System.Collections.Generic;
using GameClub.Entities.Common;
using Microsoft.AspNetCore.Identity;

namespace GameClub.Entities
{
    public  class User:IdentityUser, IDeletable
    {
    
        public bool IsDeleted { get; set; }
        
        public User():base(){}

        public User(string userName):this()
        {
            this.UserName = userName;
        }

       
    }
}