﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameClub.Entities.Common;

namespace GameClub.Entities
{
    public class Session:Identity, IDeletable
    {
        public string Id { get; set; }
        public Client Client { get; set; }
        public SessionState SessionState { get; set; }
        public IList<SessionPayment> Payments { get; set; }
      
        public bool IsDeleted { get; set; }

        public Session()
        {
            Id = Guid.NewGuid().ToString();
        }


        
    }
}