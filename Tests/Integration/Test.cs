﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tests.Integration.Common;
using Xunit;

namespace Tests.Integration
{
    public class Test: TestFixture
    {
        [Fact]
        public async Task MyTest()
        {
            var request = "/api/SampleData/WeatherForecasts";

            var a = this;
            // Act
            var response = await Client.GetAsync(request);

            
            // Assert
            //response.EnsureSuccessStatusCode();
            Assert.Equal(new Object(),await response.Content.ToObject<IEnumerable<object>>());
        }
    }
}