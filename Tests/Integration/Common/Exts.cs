﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tests.Integration.Common
{
    public static class Exts
    {
        public static StringContent GetStringContent  (this  object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.Default);
        }

        public static async Task<string> ToJsonString(this HttpContent content)
        {
            return await content.ReadAsStringAsync();
        }

        public static async Task<T> ToObject<T>(this HttpContent content)
        {
            var jsonString = await content.ToJsonString();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }
        
        
    }
}