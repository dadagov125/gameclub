﻿using GameClub.Client.Lib;
using Hardcodet.Wpf.TaskbarNotification;

using System.Windows;

namespace GameClub.Client.WpfApp
{

    
    public partial class App : Application
    {

        private TaskbarIcon notifyIcon;

        private GCClient gcClient;

        public App()
        {
            gcClient = new GCClient();
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
            gcClient.Start();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose();
            gcClient.Dispose();
            base.OnExit(e);
        }

    }
}
