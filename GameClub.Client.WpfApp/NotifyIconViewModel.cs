﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GameClub.Client.WpfApp
{
    public class NotifyIconViewModel
    {
        /// <summary>
        /// Shows a window, if none is already open.
        /// </summary>
        public ICommand ShowWindowCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () =>!Application.Current.MainWindow.IsVisible,
                    CommandAction = () =>
                    {
                        if (Application.Current.MainWindow==null)
                        {
                            Application.Current.MainWindow = new MainWindow();
                        }                       
                        Application.Current.MainWindow.Show();
                    }
                };
            }
        }

   
        public ICommand HideWindowCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => {
                        Application.Current.MainWindow.Hide();
                      
                        },
                    CanExecuteFunc = () => Application.Current.MainWindow.IsVisible
                };
            }
        }


        public ICommand ExitApplicationCommand
        {
            get
            {
                return new DelegateCommand { CommandAction = () => Application.Current.Shutdown() };
            }
        }
    }
}
