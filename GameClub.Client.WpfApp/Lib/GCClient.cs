﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using GameClub.Common.Actions;
using Makaretu.Dns;
using Microsoft.AspNetCore.SignalR.Client;

namespace GameClub.Client.Lib
{
    public class GCClient : IDisposable
    {
        private readonly string _serviceName = "game_club.service.local";
        private readonly System.Timers.Timer _timer;
        private readonly int _timerRunPeriod = 10000;
        private readonly MulticastService _multicastService;
        private HubConnection _hubConnection;


        public GCClient()
        {
            _multicastService = new MulticastService();
            _timer = new System.Timers.Timer(_timerRunPeriod);
            _timer.Elapsed += async (s, e) => await ExecuteTimerCallback(s, e);
            // _timer.AutoReset = true;
            _timer.Enabled = true;

        }

        private async Task ExecuteTimerCallback(object sender, ElapsedEventArgs e)
        {
            IList<IPAddress> ipAddresses = await FindGCServerAddresses();
            _hubConnection = await TryCreateHubConnection(ipAddresses);
            if (_hubConnection != null && _hubConnection.State == HubConnectionState.Connected)
            {
                _timer.Stop();
              // var hub= _hubConnection.CreateHubProxy("GCClientHub");
              HandleHub();
            }
        }

        private void HandleHub()
        {
            
            _hubConnection.On("Confirmed", (string message) => {
                Console.WriteLine(message);

                _hubConnection.On("Action",async (IAction action)=> {
                   await HandleAction(action);
                });
            });

            _hubConnection.On("NoConfirmed", async (string message) => {

                Console.WriteLine(message);
               await _hubConnection.DisposeAsync();
                _timer.Start();
            });

            var processorId= WinAPI.GetProcessorId();    

            _hubConnection.InvokeAsync("Confirm", $"{processorId}");
        }

        private async Task HandleAction(IAction action)
        {
            if (action == null) return;
            switch (action.Type)
            {
                case ActionTypes.TEST:
                    var data = action.GetData<string>();
                    break;
                default:
                    break;
            }
        }

        private async Task<IList<IPAddress>> FindGCServerAddresses()
        {
            Console.WriteLine("FindGCServerAddresses");

            Message request = new Message();
            request.Questions.Add(new Question { Name = _serviceName, Type = DnsType.A });
            var cancelToken = new CancellationTokenSource(5000);
            try
            {
                Message response = await _multicastService.ResolveAsync(request, cancelToken.Token);

                return response.Answers.OfType<AddressRecord>().Select(a =>
                {
                    Console.WriteLine($"Discovered IP Address of GameClub Server: {a.Address}");
                    return a.Address;
                }).ToList();
            }
            catch (TaskCanceledException e)
            {
                Console.WriteLine(e.Message);
            }
            return Array.Empty<IPAddress>().ToList();
        }

        private async Task<HubConnection> TryCreateHubConnection(IList<IPAddress> addresses)
        {
            foreach (var address in addresses)
            {
                var connection = await CreateHubConnection(address);
                if (connection.State == HubConnectionState.Connected)
                    return connection;
               await connection.DisposeAsync();
            }
            return null;
        }

        private async Task<HubConnection> CreateHubConnection(IPAddress address)
        {
            HubConnection connection = new HubConnectionBuilder()
                .WithUrl($"http://{address}:5000/GCClientHub")
                .Build();

            try
            {
                await connection.StartAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return connection;
        }

        public void Start()
        {

            try
            {
                _multicastService.Start();
                _timer.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void Dispose()
        {
            _timer.Stop();
            _timer.Dispose();
            _multicastService.Stop();
            _multicastService.Dispose();
            _hubConnection?.DisposeAsync().Wait();
        }
    }




}